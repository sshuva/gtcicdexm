FROM python:3.8-slim-buster

LABEL Name="Python CI CD Project" Version=1.0
LABEL org.opencontainers.image.source = "https://gitlab.com/sshuva/gtcicdexm"
